extends KinematicBody2D

const SPEED = 200

var velocity = Vector2()


func _ready():
	pass


func _physics_process(delta):
	move_and_slide(velocity * SPEED)

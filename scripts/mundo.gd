extends Node


func _on_Timer_timeout():
	#$nave.visible = !$nave.visible
	pass

func _on_nave_shoot(Bullet, direction, location):
    var b = Bullet.instance()
    add_child(b)
    b.rotation = direction
    b.position = location
    b.velocity = Vector2(0,1).rotated(b.rotation)